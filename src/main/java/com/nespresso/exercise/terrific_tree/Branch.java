package com.nespresso.exercise.terrific_tree;

import java.util.ArrayList;

public class Branch extends SuperBranch {

	public Branch() {
		this.length = LENGTH;
		this.leaf = false;
		this.branches = new ArrayList<Branch>();
	}
	
	@Override
	public int growingSpeed(int speed) {
		return 	speed/2;
	}
}
