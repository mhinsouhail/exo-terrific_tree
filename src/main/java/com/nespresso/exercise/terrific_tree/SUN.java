package com.nespresso.exercise.terrific_tree;

public class SUN implements ReceivedEffect {
	protected String name;
	protected int number;

	public SUN(String receivedEffectName, int receivedEffectNumber) {
		// TODO Auto-generated constructor stub
		this.name = receivedEffectName;
		this.number = receivedEffectNumber;
	}
	
	public void effectOn(SuperBranch superBranch) {
		// TODO Auto-generated method stub
		if(!superBranch.hasBranches())
			superBranch.leaf = true;
		else {
			for(SuperBranch branch : superBranch.branches) {
				branch.leaf = true;
			}
				
		}
	}

}
