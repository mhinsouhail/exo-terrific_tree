package com.nespresso.exercise.terrific_tree;

public class TerrificTree {
	private Trunk trunk = new Trunk();
	
    public TerrificTree receive(String something) {
    	trunk.receive(something);
        return this;
    }

    public String print() {
        return TerrificTreePrinter.getTerrificTreePrint(trunk);
    }
}
