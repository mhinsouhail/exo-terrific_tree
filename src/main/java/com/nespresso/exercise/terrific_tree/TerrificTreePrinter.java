package com.nespresso.exercise.terrific_tree;

public class TerrificTreePrinter {
	private static final String LENGTH ="length";
	private static final String LEAF ="leaf";
	private static final String BRANCHES ="branches";
	
	public static String getTerrificTreePrint(SuperBranch tree) {
		String res="{";
		String length = getNamePrint(LENGTH);
		res+= length+":"+tree.length;
		res+=", ";
		String leaf = getNamePrint(LEAF);
		res+= leaf+":"+tree.leaf;
		res+=", ";
		String branches = getNamePrint(BRANCHES);
		res+= branches+":";
		if(tree.branches.isEmpty())
			res+="[]";
		
		res+="}";
		
		return res ;
	}
	

	
	public static String getNamePrint(String input) {
		return "\""+input+"\"";
	}
}
