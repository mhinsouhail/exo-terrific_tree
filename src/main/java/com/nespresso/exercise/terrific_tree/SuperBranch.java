package com.nespresso.exercise.terrific_tree;

import java.util.List;

public abstract class SuperBranch {
	protected static final int LENGTH = 5;
	
	protected int length;
	protected boolean leaf;
	protected List<Branch> branches;
	
	public abstract int growingSpeed(int speed);
	
	public boolean hasBranches() {
		return (branches.size() > 0) ? true : false;
	}
}
