package com.nespresso.exercise.terrific_tree;

public interface ReceivedEffect {
	public void effectOn(SuperBranch superBranch);
}
