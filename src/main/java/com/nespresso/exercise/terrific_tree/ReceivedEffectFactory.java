package com.nespresso.exercise.terrific_tree;

public class ReceivedEffectFactory {
	private static final String WATER = "WATER";
	private static final String SUN = "SUN";
	private static final String FERTILIZER = "FERTILIZER";
	
	public static ReceivedEffect getReceivedEffect(String something) {
		String receivedEffectName = ReceivedEffectParser.getReceivedEffectName(something);
		int receivedEffectNumber = ReceivedEffectParser.getReceivedEffectNumber(something);
		
		ReceivedEffect receivedEffect = null;
		if(WATER.equals(receivedEffectName))
			receivedEffect = new Water(receivedEffectName,receivedEffectNumber);
		if(SUN.equals(receivedEffectName))
			receivedEffect = new SUN(receivedEffectName,receivedEffectNumber);
		if(FERTILIZER.equals(receivedEffectName))
			receivedEffect = new FERTILIZER(receivedEffectName);
		
		return receivedEffect;
	}
}
