package com.nespresso.exercise.terrific_tree;

import java.util.ArrayList;
import java.util.List;

public class Trunk extends SuperBranch {
	protected List<ReceivedEffect> receivedEffects;
	
	public Trunk() {
		this.length = LENGTH;
		this.leaf = false;
		this.branches = new ArrayList<Branch>();
	}
	
	public void receive(String something) {
		receivedEffects = new ArrayList<ReceivedEffect>();
		receivedEffects.add(ReceivedEffectFactory.getReceivedEffect(something));
		for(ReceivedEffect receivedEffect : receivedEffects) {
			receivedEffect.effectOn(this);
		}
	}

	@Override
	public int growingSpeed(int speed) {
		// TODO Auto-generated method stub
		return speed;
	}

}
