package com.nespresso.exercise.terrific_tree;

public class Water implements ReceivedEffect {
	protected String name;
	protected int number;
	
	public Water(String receivedEffectName, int receivedEffectNumber) {
		this.name = receivedEffectName;
		this.number = receivedEffectNumber;
	}

	public void effectOn(SuperBranch superBranch) {
		// TODO Auto-generated method stub
		superBranch.length += superBranch.growingSpeed(number);
	}

}
