package com.nespresso.exercise.terrific_tree;

public class FERTILIZER implements ReceivedEffect {
	protected String name;

	public FERTILIZER(String receivedEffectName) {
		// TODO Auto-generated constructor stub
		this.name = receivedEffectName;
	}

	public void effectOn(SuperBranch superBranch) {
		// TODO Auto-generated method stub
		boolean x = false;
		if(superBranch.length > 10)	{
			for(SuperBranch branch : superBranch.branches) {
				if(branch.length > 10) {
					branch.branches.add(new Branch());
					x = true;
				}
			}
			if(!x)
				superBranch.branches.add(new Branch());
		}
	}

}
