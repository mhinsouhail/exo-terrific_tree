package com.nespresso.exercise.terrific_tree;

public class ReceivedEffectParser {
	
	public static String getReceivedEffectName(String something) {
		return something.replaceAll("[0-9]", "").trim();			
	}
	
	public static int getReceivedEffectNumber(String something) {
		String numberString = something.replaceAll("[a-zA-Z]", "").trim();
		return Integer.parseInt(numberString);			
	}
}
